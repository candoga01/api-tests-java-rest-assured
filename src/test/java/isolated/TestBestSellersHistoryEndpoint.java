package isolated;

import config.Configuration;
import io.restassured.http.ContentType;
import org.aeonbits.owner.ConfigFactory;
import org.apache.http.HttpStatus;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import static io.restassured.RestAssured.*;
import static org.hamcrest.Matchers.equalTo;

class TestBestSellersHistoryEndpoint {

    private String token = "xDUMHspGUSuwgcXKKna27F6qsWj12NBm";

    @BeforeAll
    static void setUp() {
        Configuration configuration = ConfigFactory.create(Configuration.class);

        baseURI = configuration.baseURI();
        basePath = configuration.basePath();
    }

    @Test
    @DisplayName("When get all best-sellers history, then a filtered list of them is retrieved")
    void getAllBestSellersHistory_ThenAFilteredListOfThemIsRetrieved() {

        given()
            .contentType(ContentType.JSON)
            .queryParam("api-key", token)
        .when()
            .get("/lists/best-sellers/history.json")
        .then()
            .assertThat()
                .statusCode(HttpStatus.SC_OK)
                .body("status", equalTo("OK"))
                .body("copyright", equalTo("Copyright (c) 2021 The New York Times Company.  All Rights Reserved."))
                .body("num_results", equalTo(33606));
    }

    @Disabled("Skipped")
    @Test
    @DisplayName("When get all best-sellers history without a token, then the status code is 401")
    void getAllBestSellersHistory_ThenTheStatusCodeIs401() {

        given()
            .contentType(ContentType.JSON)
        .when()
            .get("/lists/best-sellers/history.json")
        .then()
            .assertThat()
                .statusCode(HttpStatus.SC_UNAUTHORIZED);
    }
}
