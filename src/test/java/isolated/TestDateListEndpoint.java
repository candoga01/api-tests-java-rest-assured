package isolated;

import config.Configuration;
import io.restassured.http.ContentType;
import org.aeonbits.owner.ConfigFactory;
import org.apache.http.HttpStatus;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import static io.restassured.RestAssured.*;
import static org.hamcrest.Matchers.equalTo;

class TestDateListEndpoint {

    private String token = "xDUMHspGUSuwgcXKKna27F6qsWj12NBm";

    @BeforeAll
    static void setUp() {
        Configuration configuration = ConfigFactory.create(Configuration.class);

        baseURI = configuration.baseURI();
        basePath = configuration.basePath();
    }

    @Test
    @DisplayName("When get all best-sellers by current date, then a filtered list of them is retrieved")
    void getAllBestSellersByCurrentDate_ThenAFilteredListOfThemIsRetrieved() {

        String currentDate = "current";
        String list = "hardcover-fiction";

        given()
            .contentType(ContentType.JSON)
            .queryParam("api-key", token)
        .when()
            .get("/lists/" + currentDate + "/" + list + ".json")
        .then()
            .assertThat()
                .statusCode(HttpStatus.SC_OK)
                .body("status", equalTo("OK"))
                .body("copyright", equalTo("Copyright (c) 2021 The New York Times Company.  All Rights Reserved."))
                .body("num_results", equalTo(15));
    }

    @Test
    @DisplayName("When get all best-sellers by specific date, then a filtered list of them is retrieved")
    void getAllBestSellersBySpecificDate_ThenAFilteredListOfThemIsRetrieved() {

        String specificDate = "2021-05-02";
        String list = "hardcover-fiction";

        given()
            .contentType(ContentType.JSON)
            .queryParam("api-key", token)
        .when()
            .get("/lists/" + specificDate + "/" + list + ".json")
        .then()
            .assertThat()
                .statusCode(HttpStatus.SC_OK)
                .body("status", equalTo("OK"))
                .body("copyright", equalTo("Copyright (c) 2021 The New York Times Company.  All Rights Reserved."))
                .body("num_results", equalTo(15));
    }

    @Disabled("Skipped")
    @Test
    @DisplayName("When get all best-sellers by specific date without a token, then the status code is 401")
    void getAllBestSellersBySpecificDateWithoutAToken_ThenTheStatusCodeIs401() {

        String specificDate = "2021-05-02";
        String list = "hardcover-fiction";

        given()
            .contentType(ContentType.JSON)
        .when()
            .get("/lists/" + specificDate + "/" + list + ".json")
        .then()
            .assertThat()
                .statusCode(HttpStatus.SC_UNAUTHORIZED);
    }

    @Disabled("Skipped")
    @Test
    @DisplayName("When get all best-sellers by specific date without a date, then the status code is 400")
    void getAllBestSellersBySpecificDateWithoutADate_ThenTheStatusCodeIs400() {

        String specificDate = "";
        String list = "hardcover-fiction";

        given()
            .contentType(ContentType.JSON)
            .queryParam("api-key", token)
        .when()
            .get("/lists/" + specificDate + "/" + list + ".json")
        .then()
            .assertThat()
                .statusCode(HttpStatus.SC_BAD_REQUEST);
    }

    @Disabled("Skipped")
    @Test
    @DisplayName("When get all best-sellers by specific date without a list, then the status code is 400")
    void getAllBestSellersBySpecificDateWithoutAList_ThenTheStatusCodeIs404() {

        String specificDate = "2021-05-02";

        given()
            .contentType(ContentType.JSON)
            .queryParam("api-key", token)
        .when()
            .get("/lists/" + specificDate + "/")
        .then()
            .assertThat()
                .statusCode(HttpStatus.SC_NOT_FOUND);
    }
}

